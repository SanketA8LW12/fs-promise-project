const fs = require('fs');
const path = require('path');
const dirPath = './tempDirectory'; 

function createDir(dirPath){
        return new Promise((resolve, reject) => {
            fs.mkdir(dirPath, {recursive : true}, (error) =>{
                if(error){
                    reject(error);
                }
                else{
                    console.log("dir cretaed")
                    
                    resolve();
                }
            });
        });
    }

    function createFiles(dirPath, numberOfFiles){
        return new Promise((resolve, reject) =>{
            let count = 0;
            let pathArr = [];

            for(let index = 0; index < numberOfFiles; index++){
                let fileName = `${index + 1}.json`;

                fs.writeFile(`${dirPath}/${fileName}`, "", (error, data) => {
                    if(error){
                        reject(error);
                    }
                    else{
                        count++;
                        console.log("File Created");
                        pathArr.push(fileName);

                        if(count == numberOfFiles){
                            resolve(pathArr);
                        }
                    }

                    
                })
            }
        })
    }

    function deleteFiles(dirPath, pathArr){
        
            let count = 0;
            for(let index = 0; index < pathArr.length; index++){
                fs.unlink(`${dirPath}/${pathArr[index]}`, (error) => {
                    if(error){
                        console.log(error);
                        return;
                    }
                    else{
                        console.log("File delete");
                    }
                })
            }
        
    }

    function main(dirPath, numberOfFiles){
        createDir(dirPath)
        .then(()=>{
            return createFiles(dirPath, numberOfFiles)
        }).then((pathArr)=>{
            deleteFiles(dirPath, pathArr);
        })
    }

module.exports = main;
