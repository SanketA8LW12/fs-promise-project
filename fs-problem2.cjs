const fs = require('fs');
const path = require('path');
const readFrom = path.join(__dirname, './lipsum.txt');

const  fileNameStore = path.join(__dirname, 'fileNames.txt');

let fileOne = path.join(__dirname, 'fileOne.txt');
let fileTwo = path.join(__dirname, 'fileTwo.txt');
let fileThree = path.join(__dirname, 'fileThree.txt');

function readFile(filePath){
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', function (error, data){
            if(error){
                reject(error);
            }
            else{
                console.log("file has been read");
                //console.log(data + " &&&&&&&&&&&&&&")
                resolve(data);
            }
        } )
    })
}

function writeFile(filePath, data){
    return new Promise((resolve, reject)=>{
        fs.writeFile(filePath, data+"\n", function (error){
            if(error){
                reject(error);
            }
            else{
                resolve(filePath);
            }
        })
    })
}

function appendFile(filePath)
{
    return new Promise((resolve, reject) =>{
        fs.appendFile(fileNameStore, filePath+"\n", function (error, data){
            if(error){
                reject(error);
            }
            else{
                //console.log("------",filePath);
                resolve(filePath);
            }
        })
    })
}

function deletFile(pathArr){
    
        for(let index = 0; index < pathArr.length; index++){
            fs.unlink(pathArr[index], function (error) {
                if(error){
                    console.log(error);
                }
                else{
                    console.log("File deleted");
                }
            })
        }
    
}

function main(){
    readFile(readFrom)
    .then((data) => {
        return writeFile(fileOne, data.toUpperCase());
    })
    
    .then((filePath) =>{
        return writeFile(fileNameStore, filePath);
    })
    
    .then(()=>{
        
        return readFile(fileOne);
    })
    
    .then((data) => {
    
        return writeFile(fileTwo, data.toLowerCase().split(". "));
    })
    
    .then((filePath)=>{
        return appendFile(filePath);
    })
    
    .then(() => {
        
        return readFile(fileTwo);
    })
    
    .then((data) => {
        //console.log("data " + data)
       
        return writeFile(fileThree, data);
    })
    
    .then((filePath)=>{
        return appendFile(filePath);
    })
    
    .then((filePath)=>{
        return readFile(fileNameStore);
    })
    
    .then((data) => {
        let fileArr= data.split("\n");
        fileArr.pop();
        return deletFile(fileArr)
    })
}


module.exports = main;